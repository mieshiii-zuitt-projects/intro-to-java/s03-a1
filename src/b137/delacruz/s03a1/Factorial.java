package b137.delacruz.s03a1;

import java.util.Scanner;

public class Factorial {

    public static  void main(String[] args){
        System.out.println("Factorial\n");

        System.out.println("Enter a number:\n");
        Scanner numScanner = new Scanner(System.in);

        int number = numScanner.nextInt();
        int factorial = 1;

        for (int i=1; i <= number; ++i){
            factorial = factorial * i;
        }

        System.out.println("The factorial of " + number + " is " + factorial);
    }
}
